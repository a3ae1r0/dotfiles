-- autostart.lua
local awful = require("awful")

local function run_once(cmd)
    local findme = cmd
    local firstspace = cmd:find(' ')
    if firstspace then findme = cmd:sub(0, firstspace - 1) end
    awful.spawn.with_shell(string.format(
                               'pgrep -u $USER -x %s > /dev/null || (%s)',
                               findme, cmd), false)
end

-- Term
run_once("kity")

-- Dual Monitors
run_once(
    "if ((xrandr | grep DP-1) && (xrandr | grep DP-2)); then xrandr --output DP-1 --output DP-2 --left-of DP-1; fi")

return autostart
