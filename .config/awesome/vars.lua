-- Theme handling library
local beautiful = require("beautiful")

-- {{{ Variable definitions
beautiful.useless_gap = 5

-- This is used later as the default terminal and editor to run.
terminal = "kitty"
editor = os.getenv("EDITOR") or "nano"
editor_cmd = terminal .. " -e " .. editor

-- modkey.
modkey = "Mod1"
