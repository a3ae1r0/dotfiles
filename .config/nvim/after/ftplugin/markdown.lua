vim.wo.spell = true
vim.wo.number = false
vim.wo.relativenumber = false

vim.o.tabstop = 2
vim.o.softtabstop = 2
vim.o.shiftwidth = 2
