local opts = { noremap = true, silent = true }
local term_opts = { silent = true }

local keymap = vim.api.nvim_set_keymap

-- Remap leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Normal --
-- Window movement and resize
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
-- keymap("n", "<C-l>", "<C-w>l", opts)

keymap("n", "<C-Up>", ":resize +2<CR>", opts)
keymap("n", "<C-Down>", ":resize -2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize +2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize -2<CR>", opts)

-- Navigate buffers
keymap("n", "[", ":bnext<CR>", opts)
keymap("n", "]", ":bprevious<CR>", opts)
keymap("n", "\\", ":b#<CR>", opts)

-- Insert --
keymap("i", "jk", "<ESC>", opts)

-- Removes search highlighting and redraws screen
keymap("n", "<C-l>", ":nohl<CR><C-L>", opts)

-- "Auto-complete" alternative to autopairs plugin
keymap("i", "(", "()<ESC>i", opts)
keymap("i", "{", "{}<ESC>i", opts)
keymap("i", "[", "[]<ESC>i", opts)
keymap("i", "\"", "\"\"<ESC>i", opts)
keymap("i", "\'", "''<ESC>i", opts)

-- Visual --
-- Stay in "indent mode"
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- File Explorer
keymap("n", "<leader>e", ":NvimTreeToggle<cr>", opts)

-- Markdown preview
keymap("n", "<leader>m", ":MarkdownPreview<cr>", opts)
keymap("n", "<leader>s", ":15split ~/projects/wiki/scratchpad/scratchpad.md<cr>", opts)

-- Telescope
keymap("n", "<leader>o", "<cmd>lua require'telescope.builtin'.find_files(require('telescope.themes').get_dropdown({ previewer = false }))<cr>", opts)
keymap("n", "<leader>f", "<cmd>lua require'telescope.builtin'.current_buffer_fuzzy_find()<cr>", opts) -- ... .live_grep({search_dirs={vim.fn.expand("%:p")}})
keymap("n", "<leader>g", "<cmd>lua require'telescope.builtin'.live_grep({grep_open_files=true})<cr>", opts)
keymap("n", "<leader>g", "<cmd>lua require'telescope.builtin'.live_grep()<cr>", opts)
keymap("n", "<leader>b", "<cmd>lua require'telescope.builtin'.buffers(require('telescope.themes').get_dropdown({ previewer = false }))<cr>", opts)
keymap("n", "<leader>hf", "<cmd>lua require'telescope.builtin'.oldfiles(require('telescope.themes').get_dropdown({ previewer = false }))<cr>", opts)
keymap("n", "<leader>hs", "<cmd>lua require'telescope.builtin'.search_history(require('telescope.themes').get_dropdown({ previewer = false }))<cr>", opts)
keymap("n", "<leader>hc", "<cmd>lua require'telescope.builtin'.command_history(require('telescope.themes').get_dropdown({ previewer = false }))<cr>", opts)
keymap("n", "<leader>sc", "<cmd>lua require'telescope.builtin'.spell_suggest(require('telescope.themes').get_cursor({ layout_config = {width = 30} }))<cr>", opts)
