local status_ok, lualine = pcall(require, "lualine")
if not status_ok then
  return
end

local branch = {
  "branch",
  icon = ""              -- Better icon than default
}

lualine.setup {
  options = {
    theme = 'gruvbox',
    disabled_filetypes = { "NvimTree"},
  },
  sections = {
    lualine_a = { "mode" },
    lualine_b = { branch },
    lualine_c = { "filename" },
    lualine_x = { "filetype" },
    lualine_y = { "progress" },
    lualine_z = { "location" }
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = { "filename" },
    lualine_x = { "location" },
    lualine_y = {},
    lualine_z = {}
  }
}
