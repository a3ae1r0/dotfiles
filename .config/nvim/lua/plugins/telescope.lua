local status_ok, telescope = pcall(require, "telescope")
if not status_ok then
  return
end

local actions = require("telescope.actions")
local actions_layout = require("telescope.actions.layout")

telescope.setup {
  defaults = {
    prompt_prefix = " ",
    selection_caret = " ",

    mappings = {
      i = {
        ["<C-j>"] = actions.move_selection_next,
        ["<C-k>"] = actions.move_selection_previous,

        ["<C-l>"] = actions.cycle_history_next,
        ["<C-h>"] = actions.cycle_history_prev,

        ["<C-p>"] = actions_layout.toggle_preview,

        ["<esc>"] = actions.close
      }
    }
  },
}
