local status_ok, nvim_tree = pcall(require, "nvim-tree")
if not status_ok then
  return
end

local config_status_ok, nvim_tree_config = pcall(require, "nvim-tree.config")
if not config_status_ok then
  return
end

local tree_cb = nvim_tree_config.nvim_tree_callback

nvim_tree.setup {
  -- Overwrite some default icons
  renderer = {
    icons = {
      glyphs = {
        default = "",
        git = {
          unstaged = "u",
          staged = "s",
          untracked = "",
        },
      },
    },
  },
  view = {
    mappings = {
      custom_only = false,
      list = {
        { key = { "l", "<CR>", "o" }, cb = tree_cb "edit" },
        { key = "h", cb = tree_cb "split" },
        { key = "v", cb = tree_cb "vsplit" },
      },
    },
  }
}
