-- General --
vim.opt.hidden = true
vim.opt.swapfile = false                        -- creates a swapfile
vim.opt.splitbelow = true                       -- force all horizontal splits to go below current window
vim.opt.splitright = true                       -- force all vertical splits to go to the right of current window
vim.opt.updatetime = 100                        -- faster completion (4000ms default)

-- Search --
vim.opt.incsearch = true                        -- search as characters are entered
vim.opt.hlsearch = true                         -- highlight all matches on previous search pattern
vim.opt.ignorecase = true                       -- ignore case in search patterns
vim.opt.smartcase = true                        -- smart case

-- Tab vs Space war
vim.opt.expandtab = true                        -- convert tabs to spaces
vim.opt.softtabstop = 4                         -- for spaces tab
vim.opt.shiftwidth = 4                          -- the number of spaces inserted for each indentation
vim.opt.tabstop = 4                             -- insert 2 spaces for a tab

-- UI --
vim.opt.cursorline = true                       -- highlight the current line
vim.opt.number = true                           -- set numbered lines
vim.opt.relativenumber = true                   -- set relative numbered lines
vim.opt.showmode = false                        -- hide "-- INSERT --" or "-- VISUAL --"
vim.opt.termguicolors = true

vim.cmd [[set iskeyword+=-]]                   -- count words with hifen as only one word
