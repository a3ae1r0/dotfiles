local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- Reload new config and sync packer after saving this file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  vim.notify("Packer is not available!")
  return
end

-- Nice packer popup window
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
}

-- Plugins here
return packer.startup(function(use)
  use "wbthomason/packer.nvim"
  use "morhetz/gruvbox"
  use "tpope/vim-commentary"

  -- cmpt
  use {
    "hrsh7th/nvim-cmp",
    config = [[require("plugins.cmp")]]
  }
  use "hrsh7th/cmp-buffer"
  use "hrsh7th/cmp-path"
  use "hrsh7th/cmp-cmdline"
  use "saadparwaiz1/cmp_luasnip"        -- Snippet completions
  use "L3MON4D3/LuaSnip"                -- Snippet engine

  use {
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
    config = [[require("plugins.treesitter")]]
  }

  use {
    "nvim-telescope/telescope.nvim",
    requires = {"nvim-lua/plenary.nvim", "kyazdani42/nvim-web-devicons"},
    config = [[require("plugins.telescope")]]
  }

  use {
    "lewis6991/gitsigns.nvim",
    config = [[require("plugins.gitsigns")]]
  }

  use {
    "nvim-lualine/lualine.nvim",
    requires = "kyazdani42/nvim-web-devicons",
    config = [[require("plugins.lualine")]]
  }

  use {
    'kyazdani42/nvim-tree.lua',
    requires = 'kyazdani42/nvim-web-devicons',
    config = [[require("plugins.treexplorer")]]
  }

  -- Automatically set up configuration after cloning packer.nvim
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)
