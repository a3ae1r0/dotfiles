""""""""""""""""""""""""
"        General       "
""""""""""""""""""""""""
let mapleader = "\<Space>"

set nocompatible             " no Vi-compatible (legacy)
set noswapfile               " don't create swap files
set hidden

set splitright               " new vsplit open to the right
set splitbelow               " new split open below

" tab vs space
set expandtab                " convert tabs to spaces
set tabstop=4                " four spaces tab
set softtabstop=4            " indeting is four spaces
set shiftwidth=4             " indeting is four spaces
set number                   " display line number
set relativenumber
set list
set listchars=tab:›\ ,eol:¬,trail:⋅
set autoindent               " automatically indent at new lines
set foldenable
set foldmethod=manual

" search
set incsearch                " search as characters are entered
set hlsearch                 " highlight matches
set ignorecase               " case insensitive search
set smartcase                " be smart when searching

set backspace=2              " backspace key won't move from current line
set cryptmethod=blowfish2    " set encryption

" interface
syntax on                    " enable syntax highlighting
filetype indent plugin on 
set wildmenu                 " visual tabcomplete
set cursorline
set updatetime=100           " speed up GitGutter

" statusline
set ruler                     " show cursor location at bottom screen
set laststatus=2

" colorscheme
color gruvbox
set t_Co=256
set background=dark

""""""""""""""""""""""""
"        Mapping       "
""""""""""""""""""""""""
" no bad habits
noremap  <Up>    <NOP>
inoremap <Up>    <NOP>
noremap  <Down>  <NOP>
inoremap <Down>  <NOP>
noremap  <Left>  <NOP>
inoremap <Left>  <NOP>
noremap  <Right> <NOP>
inoremap <Right> <NOP>

inoremap jk <ESC>
nnoremap [ :bnext<CR>
nnoremap ] :bprevious<CR>

inoremap ( ()<Esc>i
inoremap { {}<Esc>i
inoremap [ []<Esc>i
inoremap " ""<Esc>i
inoremap ' ''<Esc>i

nmap <leader>e :NERDTreeToggle<CR>
nmap <leader>m :MarkdownPreviewToggle<CR>

nnoremap <leader> gs :Gstatus<CR>
nnoremap <leader> gd :Gdiff<CR>
nnoremap <leader> gb :Gblame<CR>
nnoremap <leader> ga :Git add %<CR>
nnoremap <leader> gc :Git commit -F /tmp/commit.md<CR>
nnoremap <leader> gp :Git push<CR>

" FZF
nnoremap <silent> <leader>o :Files<CR>
nnoremap <silent> <leader>g :Giles<CR>
nnoremap <silent> <leader>b :Buffers<CR>
nnoremap <silent> <Leader>r :Rg<CR>
nnoremap <silent> <leader>l :Lines<CR>
nnoremap <silent> <leader>f :BLines<CR>

" allow saving when not root
cmap w!! w !sudo tee > /dev/null %

""""""""""""""""""""""""
"        Plugins       "
""""""""""""""""""""""""
let g:NERDTreeWinSize = 25
let g:NERDTreeMinimalUI = 1
let g:NERDTreeShowBookmarks = 1

" hide directory information
augroup nerdtreehidecwd
  autocmd!
  autocmd FileType nerdtree setlocal conceallevel=3
          \ | syntax match NERDTreeHideCWD #^[</].*$# conceal
          \ | setlocal concealcursor=n
augroup end

let g:fzf_layout = { 'down': '40%' }
let g:fzf_preview_window = ['right:50%', 'ctrl-/']

""""""""""""""""""""""""
"        Custom        "
""""""""""""""""""""""""
highlight BadWhitespace ctermfg=blue guibg=blue
au BufRead,BufNewFile * match BadWhitespace /\s\+$/
