/******************************************************************************************
 * sources:
 * - [All-around] arkenfox (https://github.com/arkenfox/user.js)
 * - [Privacy] PrivacyTools (https://www.privacytools.io/browsers/#about_config)
 ******************************************************************************************/

/****************
 * General
 ****************/
/* disable about:config warning */
user_pref("general.warnOnAboutConfig", false);									// XHTML version
user_pref("browser.aboutConfig.showWarning", false); 							// HTML version [FF71+]
user_pref("browser.shell.checkDefaultBrowser", false);							// disables check if it is your default browser
user_pref("browser.startup.page", 0);											// blank page at startup
user_pref("browser.startup.homepage", "about:blank");							// blank page as homepage and new tab
user_pref("intl.accept_languages", "en-US, en");								// set preferred language for displaying web pages
user_pref("app.update.auto", false);											// disables auto updates
user_pref("browser.search.update", false);										// disables search engine updates
user_pref("dom.ipc.plugins.flash.subprocess.crashreporter.enabled", false);		// disable sending Flash crash reports
user_pref("dom.ipc.plugins.reportCrashURL", false);								// disable sending the URL of the website where a plugin crashed
user_pref("extensions.getAddons.showPane", false); 								// disable about:addons' Recommendations pane (uses Google Analytics)
user_pref("extensions.htmlaboutaddons.recommendations.enabled", false)			// disable recommendations in about:addons' Extensions and Themes panes

/* disable activity stream content (top stories, sponsored content), pocket */
user_pref("browser.newtabpage.activity-stream.feeds.telemetry", false);
user_pref("browser.newtabpage.activity-stream.telemetry", false);
user_pref("browser.newtabpage.activity-stream.feeds.snippets", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false);
user_pref("browser.newtabpage.activity-stream.showSponsored", false);
user_pref("browser.newtabpage.activity-stream.feeds.discoverystreamfeed", false);

/* disable Captive Portal detection */
user_pref("captivedetect.canonicalURL", "");
user_pref("network.captive-portal-service.enabled", false);
/* disable Network Connectivity checks [FF65+] */
user_pref("network.connectivity-service.enabled", false);
user_pref("network.dns.disableIPv6", true);									// prevents (possible) leaks with VPN
user_pref("keyword.enabled", false);										// don't leak URL typos to search engine
/* disable live search suggestions */
user_pref("browser.search.suggest.enabled", false);
user_pref("browser.urlbar.suggest.searches", false);
user_pref("browser.formfill.enable", false);								// disable search and form history
/* disable browsing and download history */
user_pref("browser.taskbar.lists.enabled", false);
user_pref("browser.taskbar.lists.frequent.enabled", false);
user_pref("browser.taskbar.lists.recent.enabled", false);
user_pref("browser.taskbar.lists.tasks.enabled", false);

/****************
 * Styling
 ****************/
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);     // enable userChrome in Nightly
user_pref("ui.systemUsesDarkTheme", 1);                                     // enforce prefers-color-scheme as dark

/****************
 * Privacy
 ****************/
user_pref("privacy.resistFingerprinting", true);                        // more resistant to browser fingerprinting
user_pref("privacy.trackingprotection.enabled", true);                  // blocking tracking (i.e. GAnalytics) on privileged (protected) pages
user_pref("browser.urlbar.speculativeConnect.enabled", false);          // disable address bar autocomplete URLs
user_pref("dom.event.clipboardevents.enabled ", false);                 // disable tracking when copy/paste/cut from website
user_pref("media.gmp-widevinecdm.enabled ", false);                     // disable Widevine Content Decryption Module (Google) used for DRM HTML5 content
user_pref("media.navigator.enabled ", false);                           // websites microphone and camera status tracking
user_pref("network.cookie.cookieBehavior", 1);                          // only accept from the originating site (block third-party cookies)
user_pref("network.http.referer.XOriginPolicy", 2);                     // send Referer only when the full hostnames match
user_pref("network.http.referer.XOriginTrimmingPolicy", 2);             // only send scheme, host, and port in Referer header
user_pref("webgl.disabled", true);
user_pref("browser.sessionstore.privacy_level", 2);                     // never store extra session data (forms, scrollbar, etc.)
user_pref("beacon.enabled", false);                                     // disables sending additional analytics to web servers
user_pref("browser.safebrowsing.downloads.remote.enabled", false);      // prevents sending information (downloaded exec files) to Google Safe Browsing
user_pref("browser.safebrowsing.downloads.remote.url", "");				// Remove Google address

/*** WebRTC ***/
user_pref("media.peerconnection.enabled", true);
user_pref("media.peerconnection.turn.disable", false);
user_pref("media.peerconnection.use_document_iceservers", false);
user_pref("media.peerconnection.video.enabled", false);
user_pref("media.peerconnection.identity.timeout", 1);

/*** Disable prefetching pages ***/
user_pref("network.dns.disablePrefetch", true);
user_pref("network.predictor.enabled", true);
user_pref("network.prefetch-next", false);
user_pref("network.IDN_show_punycode", true);

/*** Telemetry ***/
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.enabled", false);
user_pref("toolkit.telemetry.server", "data:,");
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("toolkit.telemetry.newProfilePing.enabled", false);
user_pref("toolkit.telemetry.shutdownPingSender.enabled", false);
user_pref("toolkit.telemetry.updatePing.enabled", false);
user_pref("toolkit.telemetry.bhrPing.enabled", false);					// background hang reporter
user_pref("toolkit.telemetry.firstShutdownPing.enabled", false);
/* Telemetry Coverage */
user_pref("toolkit.telemetry.coverage.opt-out", true);
user_pref("toolkit.coverage.opt-out", true);
user_pref("toolkit.coverage.endpoint.base", "");
/* Health Reports */
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
/* Studies */
user_pref("app.shield.optoutstudies.enabled", false);
/* Crash reports */
user_pref("breakpad.reportURL", "");
user_pref("browser.tabs.crashReporting.sendReport", false);
