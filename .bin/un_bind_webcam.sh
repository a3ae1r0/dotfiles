#!/bin/bash

# Unbind and bind USB port (bus + device) where the USB webcam is connected.
# Just to make sure, I guess...

# Get device location
USB=$(grep Webcam -R /sys/bus/usb/devices/*/product | cut -d "/" -f6"")

# Act on argument passed
ARG=$1

if [ "$1" == "bind" ]; then
    sleep 5 && sudo sh -c "echo -n "${USB}" > /sys/bus/usb/drivers/usb/bind"
elif [ "$1" == "unbind" ]; then
    sleep 5 && sudo sh -c "echo -n "${USB}" > /sys/bus/usb/drivers/usb/unbind"
fi
